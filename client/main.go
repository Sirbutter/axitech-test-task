package main

import (
	"axitech-test-task/proto"
	"context"
	"fmt"
	"os"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	args := os.Args[1:]
	if len(args) < 3 {
		fmt.Printf("Not enough args")
		return
	}
	serverAddr := args[0]

	var opts []grpc.DialOption

	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))

	conn, err := grpc.Dial(serverAddr, opts...)
	if err != nil {
		fmt.Printf("%s", err)
		return
	}
	defer conn.Close()

	client := proto.NewDatabusServiceClient(conn)
	prm1, err := strconv.ParseFloat(args[1], 32)
	if err != nil {
		fmt.Printf("%s", err)
		return
	}

	prm2, err := strconv.ParseFloat(args[2], 32)
	if err != nil {
		fmt.Printf("%s", err)
		return
	}

	req := &proto.SendRequest{
		Prm1: float32(prm1),
		Prm2: float32(prm2),
	}
	resp, err := client.Send(context.TODO(), req)
	if err != nil {
		fmt.Printf("%s", err)
		return
	}

	fmt.Printf("%f", resp.GetResult())

}
