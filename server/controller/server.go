package controller

import (
	"axitech-test-task/proto"
	"context"
	"fmt"
)

const (
	Mode_add = "add"
	Mode_sub = "sub"
	Mode_mul = "mul"
	Mode_div = "div"
)

type DatabusServiceServerImpl struct {
	proto.UnimplementedDatabusServiceServer
	Mode string
}

func NewServer(mode string) DatabusServiceServerImpl {
	return DatabusServiceServerImpl{
		Mode: mode,
	}
}

func (dbs DatabusServiceServerImpl) Send(ctx context.Context, req *proto.SendRequest) (*proto.SendResponse, error) {
	var result float32

	switch dbs.Mode {
	case Mode_add:
		result = req.GetPrm1() + req.GetPrm2()
		fmt.Printf("%f + %f = %f", req.GetPrm1(), req.GetPrm2(), result)

	case Mode_sub:
		result = req.GetPrm1() - req.GetPrm2()
		fmt.Printf("%f - %f = %f", req.GetPrm1(), req.GetPrm2(), result)

	case Mode_mul:
		result = req.GetPrm1() * req.GetPrm2()
		fmt.Printf("%f * %f = %f", req.GetPrm1(), req.GetPrm2(), result)

	case Mode_div:
		if req.GetPrm2() == 0 {
			return nil, fmt.Errorf("error division by zero")
		}

		result = req.GetPrm1() / req.GetPrm2()
		fmt.Printf("%f / %f = %f", req.GetPrm1(), req.GetPrm2(), result)

	}

	return &proto.SendResponse{
		Result: result,
	}, nil
}

