package main

import (
	"axitech-test-task/proto"
	"axitech-test-task/server/controller"
	"fmt"
	"net"
	"os"
	"strconv"

	"google.golang.org/grpc"
)

func main() {
	args := os.Args[1:]
	if len(args) < 2 {
		fmt.Println("Not enough args")
		return
	}
	port, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Printf("%s", err)
		return
	}
	mode := args[1]
	switch mode {
	case controller.Mode_add, controller.Mode_sub, controller.Mode_mul, controller.Mode_div:
	default:
		fmt.Printf("Wrong mode")
		return
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", port))
	if err != nil {
		fmt.Printf("%s", err)
		return
	}
	var opts []grpc.ServerOption

	grpcServer := grpc.NewServer(opts...)
	proto.RegisterDatabusServiceServer(grpcServer, controller.NewServer(mode))
	fmt.Printf("server start ok. Listening to %d port\n", port )
	grpcServer.Serve(lis)
}
